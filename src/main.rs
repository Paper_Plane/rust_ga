extern crate rand;

use std::error::Error;
use std::fmt;
use std::fmt::Debug;
use std::fmt::Formatter;
use std::iter::{self, Sum};
use std::cmp::max;

type Individual = Vec<bool>;

struct Parcel {
    weight: f32,
    value: f32,
}

impl Debug for Parcel {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        writeln!(
            f,
            "Parcel has weight of {} and value of {}",
            self.weight, self.value
        )
    }
}

//impl Iterator for Parcel {
//    // existing members are unchanged
//    fn sum<S>(self) -> S where S: Sum<Self::Item> {
//        let nsum = (self.n * (self.n + 1) * (self.n + 2)) / 6;
//        let msum = (self.m * (self.m + 1) * (self.m + 2)) / 6;
//        S::sum(iter::once())
//    }
//}

fn random_parcel() -> Parcel {
    Parcel {
        weight: rand::random::<f32>(),
        value: rand::random::<f32>(),
    }
}

fn random_parcels(n: usize) -> Vec<Parcel> {
    (0..n).map(|_| random_parcel()).collect()
}

fn random_individual(n_parcels: usize) -> Individual {
    (0..n_parcels).map(|_| rand::random::<bool>()).collect()
}

fn random_individuals(n_parcels: usize, pop_size: usize) -> Vec<Individual> {
    (0..pop_size)
        .map(|_| random_individual(n_parcels))
        .collect()
}

fn genotype_to_phenotype<'a>(ind: &Individual, parcels: &'a Vec<Parcel>) -> Vec<&'a Parcel> {
    ind.iter()
        .enumerate()
        .flat_map(|(i, &v)| if v { Some(&parcels[i]) } else { None })
        .collect::<Vec<&Parcel>>()
}

fn evaluate(ind: &Individual, parcels: &Vec<Parcel>, max_weight: f32) -> f32 {
    let phenotype = genotype_to_phenotype(ind, parcels);

    let value_sum: f32 = phenotype.iter().map(|p| p.value).sum();
    let weight_sum: f32 = phenotype.iter().map(|p| p.weight).sum();

    if weight_sum > max_weight {
        0.0
    } else {
        value_sum
    }
}

fn mutate(ind: &Individual, mut_rate: f32) -> Individual {
    ind.iter()
        .map(|gene| {
            if rand::random::<f32>() < mut_rate {
                !*gene
            } else {
                *gene
            }
        })
        .collect()
}

fn cross_over(loser: &Individual, winner: &Individual, cross_over_rate: f32) -> Individual {
    loser
        .iter()
        .zip(winner.iter())
        .map(|(l, w)| {
            if rand::random::<f32>() < cross_over_rate {
                *w
            } else {
                *l
            }
        })
        .collect()
}

fn rand_usize(max: usize) -> usize {
    rand::random::<usize>() % max
}

fn main() {
    let n_parcels = 100;
    let pop_size = 100;
    let max_weight = 30.0; //rand::random::<f32>() * 10.0;

    let parcels = random_parcels(n_parcels);
    let mut pop = random_individuals(n_parcels, pop_size);

    let crossover_rate = 0.5;
    let mutation_rate = 0.01;

    //    println!("{:?}", pop[0]);
    //    println!("{:?}", mutate(&pop[0], 1.0 / n_parcels as f32));

    //    println!("{:?}", pop[0]);
    //    println!("{:?}", pop[1]);
    //    println!("{:?}", cross_over(&pop[0], &pop[1], 0.5));

    let mut highest_fitness = 0.0;
    let tourneys = 1000000;
    for t in 0..tourneys {
        let a = rand_usize(pop_size);
        let b = rand_usize(pop_size);

        let a_fitness = evaluate(&pop[a], &parcels, max_weight);
        let b_fitness = evaluate(&pop[b], &parcels, max_weight);

        let (loser_index, winner_index, winning_fitness, baby) = if a_fitness > b_fitness {
            let baby = cross_over(&pop[b], &pop[a], crossover_rate);
            (b, a, a_fitness, baby)
        } else {
            let baby = cross_over(&pop[a], &pop[b], crossover_rate);
            (a, b, b_fitness, baby)
        };

        highest_fitness = a_fitness.max(b_fitness).max(highest_fitness);

        pop[loser_index] = mutate(&baby, mutation_rate);
//        println!("highest fitness = {}", highest_fitness);
    }

    println!("total weight all parcels = {}", parcels.iter().map(|p| p.weight).sum::<f32>());
    println!("total value all parcels = {}", parcels.iter().map(|p| p.value).sum::<f32>());

//    println!("max_weight = {}", max_weight);
//    pop.iter().for_each(|i| println!("{}", evaluate(&i, &parcels, max_weight)));
}
